﻿using UnityEditor;
using UnityEngine;
using System.Collections;

public class CardGenerator : EditorWindow {
	
	private GameObject prefab;
	public GameObject Prefab {
		get {
			return prefab;
		}
		set {
			if(value.GetComponent<Card>() != null) {
				prefab = value;
			}
		}
	}
	
	private GameObject cardPrefab;
	
	public CardType type;
	public string cardName;
	public int attack = 0;
	public int health = 0;
	
	public string buttonName;

	[MenuItem("Window/Card Generator")]
    static void CreateWindow() {
		CardGenerator window = (CardGenerator) EditorWindow.GetWindow(typeof(CardGenerator));
		window.Show();
	}
	
	void OnGUI() {
		cardPrefab = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Card.prefab", typeof(GameObject)) as GameObject;
			
		GUILayout.Label("Existing Card", EditorStyles.boldLabel);
			Prefab = EditorGUILayout.ObjectField(prefab, typeof(GameObject), false) as GameObject;
			
		GUILayout.Label("Card Settings", EditorStyles.boldLabel);
			cardName = EditorGUILayout.TextField("Card Name", cardName); 
			type = (CardType) EditorGUILayout.EnumPopup("Card Type", type);
			
			switch (type) {
				case CardType.Immortal:
					attack = EditorGUILayout.IntField("Attack", attack);
					health = EditorGUILayout.IntField("Health", health);
					break;
				case CardType.Adventurer:
					attack = EditorGUILayout.IntField("Attack", attack);
					break;
				case CardType.Weapon:
					attack = EditorGUILayout.IntField("Attack", attack);
					break;
				case CardType.Spell:
					break;
				default:
					break;
			}
		
		if(prefab == null) {
			buttonName = "Create";
			if(GUILayout.Button(buttonName)) {
				CreatePrefab();
			}
		} else {
			buttonName = "Apply";
			if(GUILayout.Button(buttonName)) {
				EditPrefab();
			}
		}

	}
	
	private void CreatePrefab() {
		string prefabPath = GetPrefabPath();
		
		GameObject temp = PrefabUtility.InstantiatePrefab(cardPrefab) as GameObject;
		PrefabUtility.DisconnectPrefabInstance(temp);
		//Add components to temp;
		foreach(Component c in cardPrefab.GetComponents<Component>()) {
			if(temp.GetComponent(c.GetType()) == null) {
				temp.AddComponent(c.GetType());
			}
		}
		
		PrefabUtility.CreatePrefab(prefabPath, temp);
		Object.DestroyImmediate(temp, false);
	}
	
	private void EditPrefab() {
		//string prefabPath = GetPrefabPath();
		
		AssetDatabase.StartAssetEditing();
		//Change components
		AssetDatabase.StopAssetEditing();
	}
	
	private string GetPrefabPath() {
		return "Assets/Prefabs/Cards/" + cardName + ".prefab";
	}
}
