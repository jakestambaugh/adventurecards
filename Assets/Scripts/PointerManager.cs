﻿using UnityEngine;
using System.Collections.Generic;

public class PointerManager {

	private static List<Pointer> list = new List<Pointer>();
	
	public static int RegisterPointer(Pointer p) {
		 list.Add(p);
		 return list.IndexOf(p);
	}
	
	// Assume for this build that the only index being used is 0, which is the mouse;
	public static Transform GetPointerLocation(int index) {
		return GetPointerLocation(list[index]);
	}
	
	public static Transform GetPointerLocation(Pointer p) {
		return p.transform;
	}
	
	public static RaycastHit GetPointerLastHit(int index) {
		return GetPointerLastHit(list[index]);
	}
	
	public static RaycastHit GetPointerLastHit(Pointer p) {
		return p.LastHit;
	}
}
