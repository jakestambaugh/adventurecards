﻿using UnityEngine;
using System.Collections;

public class Pointer : MonoBehaviour {

    private RayCaster raycaster;
	public RayCaster Raycaster {
		get {
			return raycaster;
		}
	}
    public LayerMask layerMask;
    public float maxDistance = 100f;
	
	private Camera cam;
	
	public RaycastHit LastHit {
		get {
			return Raycaster.hit;
		}
	}
	
    
	void Awake() {
		PointerManager.RegisterPointer(this);
	}

	// Use this for initialization
	void Start () {
		cam = Camera.main; 
	    raycaster = new RayCaster();
		raycaster.LayerMask = layerMask.value;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 start = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane);
		Vector3 end = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.farClipPlane);
		
        this.transform.position = cam.ScreenToWorldPoint(start);
		Vector3 endSpace = cam.ScreenToWorldPoint(end);
		this.transform.LookAt(endSpace);
				
		raycaster.StartPosition = this.transform.position;
		raycaster.EndPosition = endSpace;
		raycaster.CastRay();
	}
	
	public class RayCaster {
		public Vector3 StartPosition;
		public Vector3 EndPosition;
		public float RayLength = 400.0f;
		public int LayerMask = 0;

		private Collider previous;
		public RaycastHit hit = new RaycastHit();

		public bool CastRay() {
			Ray ray = new Ray(StartPosition, EndPosition - StartPosition);

			Physics.Raycast(ray, out hit, RayLength, LayerMask);
			Debug.DrawRay(ray.GetPoint(0.0f), ray.direction * RayLength, Color.cyan);
			
			ProcessCollision(hit.collider);
			return hit.collider != null ? true : false;
		}

		public bool CastLine() {
			Physics.Linecast(StartPosition, EndPosition, out hit, LayerMask);
			ProcessCollision(hit.collider);
			return hit.collider != null ? true : false;
		}

		private void ProcessCollision(Collider current) {
			
			if(current != null) {
				if(Input.GetMouseButtonDown(0)) {
					current.SendMessage("OnPointerDown", SendMessageOptions.DontRequireReceiver);
				}
				if(Input.GetMouseButtonUp(0)) {
					current.SendMessage("OnPointerUp", SendMessageOptions.DontRequireReceiver);
				}
			}
			
			// No collision this frame.
			if (current == null) {
				// But there was an object hit last frame.
				if (previous != null) {
					previous.SendMessage("OnPointerExit", SendMessageOptions.DontRequireReceiver);
				}
			}

			// The object is the same as last frame.
			else if (previous == current) {
				if(Input.GetMouseButton(0)) {
					current.SendMessage("OnPointerDrag", SendMessageOptions.DontRequireReceiver);
				}
				current.SendMessage("OnPointerStay", SendMessageOptions.DontRequireReceiver);
			}

			// The object is different than last frame.
			else if (previous != null) {
				previous.SendMessage("OnPointerExit", SendMessageOptions.DontRequireReceiver);
				current.SendMessage("OnPointerEnter", SendMessageOptions.DontRequireReceiver);
			}

			// There was no object hit last frame.
			else {
				current.SendMessage("OnPointerEnter", SendMessageOptions.DontRequireReceiver);
			}

			// Remember this object for comparing with next frame.
			previous = current;
		}
	}
}
