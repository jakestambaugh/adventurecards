﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Hand : MonoBehaviour {

    public float spacer = 0.1f;
    public int maxCards = Constants.MaxHandSize;

	private List<Card> hand;
    
	void Start () {
	    hand = new List<Card>();
        // Debug.Log(hand.Count);
	}
    
    public void AddCard(Card c) {
        if(hand.Count < maxCards) {
            hand.Add(c);
            c.drag.isDraggable = true;
        }
        CardLayout.RedistributeCards(this.transform, hand, spacer);
    }
    
    public void RemoveCard(Card c) {
        if(hand.Contains(c)) {
            hand.Remove(c);
        }
        CardLayout.RedistributeCards(this.transform, hand, spacer);
    }
}
