﻿using UnityEngine;
using System.Collections;

public class Draggable : MonoBehaviour {

    public float moveSpeed = 1.0f;		// Speed at which the object follows the pointer
	public float lookSpeed = 10.0f;		// Speed at which the object rotates towards its rotation target
    public float smoothing = 7.0f;		// Smoothing factor to make the following seem natural
	public float hoverDistance = 1.0f;	// Distance from its starting position that the object moves while hovered
	public float holdDistance = 10.0f;	// Distance from the pointer that the object stays while held 

    private Vector3 target;
	public Vector3 Target {
        get {
            return target;
        }
        set {
			if(isDraggable) {
            	target = value;
            	StopCoroutine("MoveTowards");
            	StartCoroutine("MoveTowards", moveSpeed);
			}
        }
    }
	
	private Quaternion rotationTarget;
	public Quaternion RotationTarget {
		get {
			return rotationTarget;
		}
		set {
			if(isDraggable) {
				rotationTarget = value;
				StopCoroutine("LookTowards");
				StartCoroutine("LookTowards", lookSpeed);
			}
		}
	}

    private Vector3 rootTarget;
	private Quaternion rootRotation;
	
	public bool isDraggable = false;
	private bool isHeld = false;
	private Vector3 lastHeldPosition;

	private IEnumerator MoveTowards(float speed) {
		while(Vector3.Distance(transform.position, target) > 0.05f) {
            transform.position = Vector3.Lerp(transform.position, target, smoothing * Time.deltaTime);
            
            yield return null;
        }
	}
	
	private IEnumerator LookTowards(float speed) {
		while (Quaternion.Angle(transform.rotation, rotationTarget) > 1.0f) {
			transform.rotation = Quaternion.Lerp(transform.rotation, rotationTarget, smoothing * Time.deltaTime);
			
			yield return null;
		}
	}
	
	void Start() {
		// Set the rootTarget or "magnetic pull/resting position" to the transform of the object in the scene
		rootTarget = this.transform.position;
		Target = rootTarget;
		
		// Do the same for the rotation
		rootRotation = this.transform.rotation;
		RotationTarget = rootRotation;
	}
	
	void Update() {
		Debug.DrawRay(transform.position, Vector3.forward*5f, Color.green);
		if(isHeld) {
			Transform pointer = PointerManager.GetPointerLocation(0);
			Target = pointer.position + (pointer.forward * holdDistance);
			RotationTarget = Quaternion.LookRotation(Camera.main.transform.position - Target); 
		}
	}
	
	public void SetRootTarget(Vector3 pos) {
		rootTarget = pos;
	}
	
	public void SetRootRotation(Quaternion rot) {
		rootRotation = rot;
	}
	
	void OnPointerEnter() {
		if(!enabled) return;
		
		// Move the target towards the camera for hovering
		Target = Target + Vector3.Normalize(Camera.main.transform.position - Target) * hoverDistance;
		RotationTarget = Quaternion.LookRotation(Camera.main.transform.position - Target);
	}
	
	void OnPointerExit() {
		if(!enabled) return;
		
		// Move the target back towards its resting position
		Target = rootTarget;
		if(!isHeld) {
			RotationTarget = rootRotation;
		}
	}
	
	void OnPointerDown() {
		if(!enabled) return;
		
		// Toggle the hold state of the card
		isHeld = !isHeld;
		lastHeldPosition = PointerManager.GetPointerLastHit(0).point;
	}
	
	void OnPointerUp() {
		if(!enabled) return;
		
		bool moved = PointerHasMoved();
		if(moved || !isHeld) {
			DropOnto();
			isHeld = false;
			Target = rootTarget;
		}
	}
	
	private bool PointerHasMoved() {
		Vector3 hitPoint = PointerManager.GetPointerLastHit(0).point;
		float xDiff = Mathf.Abs(lastHeldPosition.x - hitPoint.x);
		float yDiff = Mathf.Abs(lastHeldPosition.y - hitPoint.y);
		bool moved = xDiff > 1.0f || yDiff > 1.0f;
		return moved;
	}
	
	private void DropOnto() {
		RaycastHit hit;
		
		if(Physics.Raycast(transform.position, Vector3.forward, out hit)) {
			print("object is " + hit.collider.gameObject);
			SendMessage("OnDrop", hit.collider.gameObject);
		}
	}
}
