﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BuildDeck : MonoBehaviour {

    public void Build() {
        List<string> potentialCards = CardDB.ToList();
        
        string s = "";
        for(int i = 0; i < Constants.StartingDeckSize; i++) {
            s = s + potentialCards[Random.Range(0, potentialCards.Count)] + "\n";
        }
        System.IO.File.WriteAllText(Application.persistentDataPath + "/deck.txt", s);
    }
}
