﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Draggable))]
public class Card : MonoBehaviour {

    public enum Location {
        InDeck,
        InHand,
        EnteringPlay,
        InPlay,
        Dying,
        Attacking
    }
    
    public CardArchetype archetype;
    public Location location;
    public Draggable drag;
    public Vector3 Target {
        get {
            return drag.Target;
        }
        set {
            drag.Target = value;
        }
    }
    
    public Quaternion RotationTarget {
        get {
            return drag.RotationTarget;
        }
        set {
            drag.RotationTarget = value;
        }
    }
    
    void OnEnable() {
        EventManager.Subscribe(Constants.PlayCard, PlayCallback);
    }
    
    void OnDisable() {
        EventManager.Unsubscribe(Constants.PlayCard, PlayCallback);
    }
    
    public static Card parseFromString(string cardSerialization, Transform cardPrefab) {
        Transform cardObject = Instantiate(cardPrefab);
        Card c = cardObject.GetComponent<Card>();
        c.archetype = CardDB.GetArchetypeFromSerialization(cardSerialization);
        return c;
    }

    void Awake() {
        drag = GetComponent<Draggable>();
    }

	void Start () {
	    location = Location.InDeck;
        Canvas c = GetComponentInChildren<Canvas>();
        Text t = c.GetComponentInChildren<Text>();
        t.text = archetype.name;
	}
    
    public void MoveAndSetTargets(Transform t) {
        transform.position = t.position;
        transform.rotation = t.rotation;
        SetRoots(t.position, t.rotation);
    }
    
    public void Draw() {
        Debug.Log("Drawing card");
        if(location == Location.InDeck) {
            location = Location.InHand;
        }
    }
    
    public void PlayCallback(Card c) {
        if(c == this && location == Location.InHand) {
            PlayArea pa = GameObject.Find("Play Area").GetComponentInChildren<PlayArea>();
            Hand h = GameObject.Find("Hand Area").GetComponentInChildren<Hand>();
            
            h.RemoveCard(this);
            pa.AddCard(this);
            
            location = Location.InPlay;
        }
    }
    
    
    public void SetRoots(Vector3 pos) {
        SetRoots(pos, null);
    }
    
    public void SetRoots(Quaternion rot) {
        SetRoots(null, rot);
    }
    
    public void SetRoots(Vector3? pos, Quaternion? rot) {
        if(pos.HasValue) {
            drag.SetRootTarget(pos.Value);
            drag.Target = pos.Value;
        }
        if(rot.HasValue) {
            drag.SetRootRotation(rot.Value);
            drag.RotationTarget = rot.Value;
        }
    }
}
