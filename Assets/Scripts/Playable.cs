﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Card))]
public class Playable : MonoBehaviour {

	public float distance = 10.0f;

	void OnDrop(GameObject obj) {
		if(obj.name == "Play Area") {
			EventManager.Publish(Constants.PlayCard, this.GetComponent<Card>());
		}
	}
}
