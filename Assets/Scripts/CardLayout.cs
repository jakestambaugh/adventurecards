﻿using UnityEngine;
using System.Collections.Generic;

public class CardLayout {
	public static void RedistributeCards(Transform beginning, List<Card> list, float spacer) {
        Vector3 cardPosition = beginning.position;
        Quaternion cardRotation = Quaternion.LookRotation(-beginning.forward);
        
        cardPosition.x -= spacer * (list.Count / 2f);
        foreach(Card c in list) {
            cardPosition.x += spacer;
            c.SetRoots(cardPosition, cardRotation);
        }
    }
}
