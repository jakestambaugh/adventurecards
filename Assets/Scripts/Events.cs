﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class CardEvent : UnityEvent<Card> { }
public class TurnEvent : UnityEvent<Player> { }
