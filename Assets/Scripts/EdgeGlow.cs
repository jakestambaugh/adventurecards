﻿using UnityEngine;
using System.Collections;

public class EdgeGlow : MonoBehaviour {

	Card card;
	Material m;
	
	void Start() {
		card = GetComponentInParent<Card>();
		m = GetComponent<Renderer>().material;
		Debug.Log("Material is " + m);
	}
	
	void Update() {
		switch(card.location) {
			case Card.Location.InHand:
				m.color = Color.yellow;
				break;
			case Card.Location.InPlay:
				m.color = Color.blue;
				break;
			default:
				m.color = Color.clear;
				break;
		}
	}
}
