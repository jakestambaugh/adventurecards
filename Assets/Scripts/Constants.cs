﻿public class Constants {
    public const int StartingDeckSize = 30;
    public const int MaxHandSize = 10;
    
    // Events that can be subscribed to
    public const string PlayCard = "PlayCard";
    
    public const string StartTurn = "StartTurn";
    public const string EndTurn = "EndTurn";
}
