﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	private int hitpoints = 10;
	public int Hitpoints {
		get {
			return hitpoints;
		}
		set {
			if(value > 0) {
				hitpoints = value;
			} else {
				hitpoints = 0;
			}
		}
	}
	
	public bool Alive = true; 
	
	public void TakeDamage(int damage) {
		Hitpoints -= damage;
		if(Hitpoints == 0) {
			Alive = false;
		}
	}
}
