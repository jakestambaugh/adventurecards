﻿using UnityEngine;
using System.Collections;

// THe Game Manager
public class Jake : MonoBehaviour {

    public Monarch monarch;
    public Immortal immortal;
    
    bool monarchTurn = true;
	
	void Start () {
        
	}
	
	void NextTurn () {
	    if(monarchTurn) {
            monarch.TakeTurn();
        } else {
            immortal.TakeTurn();
        }
        monarchTurn = !monarchTurn;
	}
}
