﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Health))]
public class Adventurer : MonoBehaviour {
    
    private Health health;
    int power = 2;
    
    void Start() {
        health = GetComponent<Health>();
    }
    
    public void Attack(Health target) {
        target.TakeDamage(power);
    }
}
