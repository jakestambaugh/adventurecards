﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Monarch : Player {
    
    private uint gold = 0;
    public uint Gold {
        get {
            return gold;
        }
        set {
            gold = value;
        }
    }
    
    public Adventurer[] party = new Adventurer[3];

	private float spacer = 1.25f;
    
    void Start() {
        PopulateDeck();
        
        List<Adventurer> list = new List<Adventurer>();
        for(int i = 0; i < party.Length; i++) {
			list.Add (party [i]);
        }
		CardLayout.RedistributeCards (this.transform, list as List<Card>, spacer);
    }
    
    void OnEnable() {
        EventManager.Subscribe(Constants.StartTurn, StartTurnCallback);
        EventManager.Subscribe(Constants.EndTurn, EndTurnCallback);
    }
    
    void OnDisable() {
        EventManager.Unsubscribe(Constants.StartTurn, StartTurnCallback);
        EventManager.Unsubscribe(Constants.EndTurn, EndTurnCallback);
    }
    
    public void TakeTurn() {
        EventManager.Publish(Constants.StartTurn, null);
    }
    
    private void StartTurnCallback(Card c) {
        
    }
    
    private void EndTurnCallback(Card c) {
        
    }
    
    private void PopulateDeck() {
        Deck deck = FindObjectOfType<Deck>();
        if(deck != null) {
            
        }
    }
}
