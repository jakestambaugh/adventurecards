﻿using UnityEngine;
using System.Collections;

public class EventTriggerTest : MonoBehaviour {
	
	// Update is called once per frame
	public void SendTurnStart() {
        Debug.Log("Published message");
        EventManager.Publish(Constants.StartTurn, null);
	}
}
