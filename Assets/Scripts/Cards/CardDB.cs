﻿using UnityEngine;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class CardDB {
	private static Dictionary<string, CardArchetype> dictionary;
	private const string databasePath = "Cardlist";
	
	private static bool loaded = false;
	
	public static void LoadDictionary() {
		dictionary = new Dictionary<string, CardArchetype>();
		
		TextAsset cardFile = Resources.Load(databasePath) as TextAsset;
		string fileString = cardFile.text;
		string[] lines = Regex.Split(fileString, "\n|\r|\r\n");
		
		foreach (string line in lines) {
			dictionary.Add(line, new CardArchetype(line));
		}
	}
	
	public static CardArchetype GetArchetypeFromSerialization(string serialization) {
		if(!loaded) {
			LoadDictionary();
		}
		CardArchetype arch;
		dictionary.TryGetValue(serialization, out arch);
		return arch;
	}
	
	public static List<string> ToList() {
		List<string> retList = new List<string>();
		foreach(string key in dictionary.Keys) {
			retList.Add(key);
		}
		return retList;
	}
}
