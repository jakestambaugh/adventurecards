﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class EventManager : MonoBehaviour {

	private Dictionary<string, CardEvent> cardEventDictionary;
    
	private static EventManager eventManager;
	public static EventManager instance {
		get {
			if(eventManager == null) {
				eventManager = FindObjectOfType(typeof(EventManager)) as EventManager;
				if(eventManager == null) {
                     // Debug.LogError("There needs to be one active EventManager script on a gameObject in your scene");
                }
            }
			if(eventManager.cardEventDictionary == null) {
				eventManager.Init();
			}
            Debug.Assert(eventManager.cardEventDictionary != null);
			return eventManager;
		}
	}
	
	void Init() {
        // Debug.Log("Resetting dictionary");
		cardEventDictionary = new Dictionary<string, CardEvent>();
	}
	
	public static void Subscribe (string eventName, UnityAction<Card> callback) {
		CardEvent thisEvent = null;
		if (instance.cardEventDictionary.TryGetValue(eventName, out thisEvent)) {
			thisEvent.AddListener(callback);
		} else {
            thisEvent = new CardEvent();
            thisEvent.AddListener(callback);
            instance.cardEventDictionary.Add(eventName, thisEvent);
        }
        // Debug.Log("Dictionary contains " + eventManager.cardEventDictionary.Count + " callbacks");
	}
    
    public static void Unsubscribe (string eventName, UnityAction<Card> callback) {
        if(eventManager == null) return;
        CardEvent thisEvent = null;
        if(instance.cardEventDictionary.TryGetValue(eventName, out thisEvent)) {
            thisEvent.RemoveListener(callback);
        }
    }
    
    public static void Publish(string eventName, Card card) {
        CardEvent thisEvent = null;
        if(instance.cardEventDictionary.TryGetValue(eventName, out thisEvent)) {
            thisEvent.Invoke(card);
        }
    }
}
