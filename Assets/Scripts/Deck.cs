﻿using UnityEngine;
using UnityEngine.Events;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class Deck : MonoBehaviour {
    
    private Stack<Card> cards;
    private Transform nextCardSpot;
    public Transform cardPrefab;
    public Hand hand;
    public float cardDistance = 0.1f;

    private static Transform instance; 
    public static Transform CardPrefab
    {
        get
        {
            return instance;
        }
    }
    
    void Awake() {
        instance = cardPrefab;
    }
    
    void Start() {
        nextCardSpot = transform;
        
        // New game instantiation
        Card[] cardlist = GenerateDeckFromFile("deck.txt");
        
        // Shuffle
        Shuffle(ref cardlist);
        
        foreach(Card card in cardlist) {
            card.MoveAndSetTargets(nextCardSpot);
            
            nextCardSpot.position = new Vector3(nextCardSpot.position.x, nextCardSpot.position.y, nextCardSpot.position.z - cardDistance);
        }
        
        cards = new Stack<Card>(cardlist);
    }
    
    void OnEnable() {
        EventManager.Subscribe(Constants.StartTurn, Draw);
    }
    
    void OnDisable() {
        EventManager.Unsubscribe(Constants.StartTurn, Draw);
    }
    
    void Draw(Card c) {
        if(cards.Count > 0) {
            Card card = cards.Pop();
            hand.AddCard(card);
            card.Draw();
        }
    }
    
    private Card[] GenerateDeckFromFile(string filename) {
        string[] lines = File.ReadAllLines(Application.persistentDataPath + "/" + filename);
        List<Card> decklist = new List<Card>();
        foreach(string line in lines) {
            // Maybe don't instantiate all of them? Pick a new location? Idk.
            Card card = Card.parseFromString(line, CardPrefab);
            decklist.Add(card);
        }
        return decklist.ToArray();
    }
    
    // Shuffles the deck using the Knuth-Fisher-Yates shuffle
    private void Shuffle(ref Card[] cardlist) {
        for(int i = 0; i < cardlist.Length; i++) {
            swap(ref cardlist[i], ref cardlist[Random.Range(i, cardlist.Length)]);
        }
    }
    
    private void swap(ref Card a, ref Card b)
    {
        Card t = a;
        a = b;
        b = t;
    }
}
